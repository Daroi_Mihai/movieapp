package com.pentastagiu.movieapp;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


public class RegisterActivity extends AppCompatActivity {

    public static final String PREFERENCE = "preference";
    public static final String PREF_NAME = "name";
    public static final String PREF_EMAIL = "email";
    public static final String PREF_PASSWD = "passwd";
    public static final String PREF_REPASSWD = "repasswd";
    public static final String PREF_ADVENTURE = "adventure";
    public static final String PREF_BIRTHDATE = "birthdate";

    private EditText user, email, password, repassword;
    private Button registerButton, birthdateButton;
    private Spinner adventure;
    private String userStr, emailStr, passwordStr, repasswordStr, adventureStr, birthdateStr;
    private int year = 2000;
    private int month;
    private int day;
    static final int DATE_DIALOG_ID = 999;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        user = (EditText) findViewById(R.id.regName);
        email = (EditText) findViewById(R.id.regEmail);
        password = (EditText) findViewById(R.id.regPass);
        repassword = (EditText) findViewById(R.id.regRePass);
        adventure = (Spinner) findViewById(R.id.advSelect);
        birthdateButton = (Button) findViewById(R.id.regBirthdate);
        registerButton = (Button) findViewById(R.id.regButton);


        birthdateListener();
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validUserData()) {
                    SharedPreferences mSharedPreference = getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
                    SharedPreferences.Editor mEditor = mSharedPreference.edit();
                    mEditor.putString(PREF_NAME, userStr);
                    mEditor.putString(PREF_EMAIL, emailStr);
                    mEditor.putString(PREF_PASSWD, passwordStr);
                    mEditor.putString(PREF_REPASSWD, emailStr);
                    mEditor.putString(PREF_ADVENTURE, adventureStr);
                    mEditor.putString(PREF_BIRTHDATE, birthdateStr);
                    mEditor.apply();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Register Error", Toast.LENGTH_SHORT);
                }
            }
        });

        String[] adventureList = new String[]{"Action", "Comedy", "Animation", "Horror"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, adventureList);
        adventure.setAdapter(adapter);
        adventureStr = adventure.getSelectedItem().toString();

    }

    private boolean validUserData() {
        userStr = user.getText().toString().trim();
        emailStr = email.getText().toString().trim();
        passwordStr = password.getText().toString().trim();
        repasswordStr = repassword.getText().toString().trim();
        if (userStr.equals("") || userStr.length() <= 4) {
            Toast.makeText(getApplicationContext(), "Name Error", Toast.LENGTH_SHORT).show();
            return false;
        } else if (emailValidation(emailStr) == false) {
            Toast.makeText(getApplicationContext(), "Email Error", Toast.LENGTH_SHORT).show();
            return false;
        } else if (passwordStr.equals("") || passwordStr.length() <= 4) {
            Toast.makeText(getApplicationContext(), "Password Error", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!(repasswordStr.equals(passwordStr))) {
            Toast.makeText(getApplicationContext(), "Password didn't match.", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public static boolean emailValidation(CharSequence email) {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    public void birthdateListener() {

        birthdateButton = (Button) findViewById(R.id.regBirthdate);

        birthdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);

            }
        });
    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, datePickerListener, year, month, day);

        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            birthdateStr = (new StringBuilder().append(day).append("-").append(month + 1)
                    .append("-").append(year)
                    .append(" ")).toString();
            birthdateButton.setText(birthdateStr);
        }
    };


}

