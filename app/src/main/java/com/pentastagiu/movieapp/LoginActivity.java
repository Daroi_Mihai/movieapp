package com.pentastagiu.movieapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private EditText email, password;
    private Button loginButton,registerButton;
    private String emailStr, passwordStr;
    private SharedPreferences mSharedPreferences;
    public static final String PREFERENCE= "preference";
    public static final String PREF_EMAIL = "email";
    public static final String PREF_PASSWD = "passwd";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mSharedPreferences = getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);

            email = (EditText)findViewById(R.id.loginEmail);
            password = (EditText)findViewById(R.id.loginPass);
            loginButton = (Button)findViewById(R.id.loginButton);
            registerButton=(Button) findViewById(R.id.registerButton) ;

            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(validUserData()){
                        if(mSharedPreferences.contains(PREF_EMAIL) && mSharedPreferences.contains(PREF_PASSWD)){
                            SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                            Intent intent = new Intent(LoginActivity.this,ListActivity.class);
                            Toast.makeText(getApplicationContext(),"You have been successfully logged in.",Toast.LENGTH_LONG).show();
                            startActivity(intent);
                            finish();
                        }}
                }
            });

            registerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                    startActivity(intent);
                }
            });

        }

    private boolean validUserData() {
        emailStr = email.getText().toString().trim();
        passwordStr = password.getText().toString().trim();
        if (!(emailStr.equals(mSharedPreferences.getString(PREF_EMAIL, null).trim()))) {
            Toast.makeText(getApplicationContext() ,"Incorrect email address.",Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!(passwordStr.equals(mSharedPreferences.getString(PREF_PASSWD, null).trim()))) {
            Toast.makeText(getApplicationContext() ,"Password incorrect.",Toast.LENGTH_SHORT).show();
            return false;
        }
        return !(emailStr.isEmpty() || passwordStr.isEmpty());
    }
}